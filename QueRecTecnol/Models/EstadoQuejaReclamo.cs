﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QueRecTecnol.Models
{
    public class EstadoQuejaReclamo
    {
        [Key]
        public int EstadoQueRec_ID { get; set; }

        [Required]
        [Display(Name = "Descripción del estado")]
        public string Descripcion { get; set; }

        [Display(Name = "Considerado cerrado")]
        public bool CierraTiquet { get; set; }
    }
}