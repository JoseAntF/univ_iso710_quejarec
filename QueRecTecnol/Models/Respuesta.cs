﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace QueRecTecnol.Models
{
    public class Respuesta
    {
        [Key]
        public int RespuestaID { get; set; }

        [Required]
        [Display(Name = "Encargado")]
        public int EmpleadoID { get; set; }
        [ForeignKey("EmpleadoID")]
        virtual public Empleado Empleado { get; set; }

        [Display(Name = "Departamento emisor")]
        public int DepartamentoID { get; set; }
        [ForeignKey("DepartamentoID")]
        virtual public Departamento Departamento { get; set; }

        [Required]
        [Display(Name = "Queja o reclamación")]
        public int QuejaRecID { get; set; }
        [ForeignKey("QuejaRecID")]
        virtual public QuejaReclamoBase QuejaReclamo { get; set; }

        [Display(Name = "Fecha de respuesta")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime FechaRespuesta { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string Detalle { get; set; }

        public string Inconformidad { get; set; }

        [Range(0, 5)]
        public int? Valoracion { get; set; }
    }
}