﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace QueRecTecnol.Models
{
    public class CompDbContext : IdentityDbContext<ApplicationUser>
    {

        public CompDbContext()
    : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static CompDbContext Create()
        {
            return new CompDbContext();
        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Departamento> Departamentos { get; set; }
        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<QuejaReclamoBase> QuejasReclamos { get; set; }
        public DbSet<Respuesta> Respuestas { get; set; }
        public DbSet<Pais> Paises { get; set; }
        public DbSet<TipoQuejaReclamo> TipoQuejasReclamos { get; set; }
        public DbSet<Oficina> Oficinas { get; set; }
        public DbSet<EstadoCliente> EstadosClientes { get; set; }
        public DbSet<EstadoQuejaReclamo> EstadoQuejaReclamo { get; set; }
        public DbSet<TipoIdentificacion> TipoIdentificacion { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            /// Detener eliminación en cascada (problemático con Respuesta y Empleado)
            modelBuilder.Entity<Respuesta>()
            .HasRequired(s => s.Empleado)
            .WithMany()
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<Respuesta>()
            .HasRequired(s => s.QuejaReclamo)
            .WithMany()
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<Cliente>()
                .HasRequired(s => s.Oficial)
                .WithMany()
                .WillCascadeOnDelete(false);
        }
    }
}