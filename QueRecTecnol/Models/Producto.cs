﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QueRecTecnol.Models
{

    public enum EstatusProducto
    {
        [Display(Name = "Nuevo")]
        NUEVO,
        [Display(Name = "Usado")]
        USADO,
        [Display(Name = "Refurb")]
        REFURBISHED,
        [Display(Name = "Defectuoso")]
        DEFECTUOSO
    }
    public class Producto
    {
        [Key]
        public int ProductoID { get; set; }

        [Required]
        public string Nombre { get; set; }

        [Required]
        public double Precio { get; set; }

        [Required]
        public EstatusProducto Estado { get; set; }
    }
}