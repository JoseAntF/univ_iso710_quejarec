﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QueRecTecnol.Models
{
    public class TipoIdentificacion
    {
        [Key]
        public int TipoIdentificacionID { get; set; }

        [Required]
        [Display(Name = "Nombre de Identificación")]
        public string Descripcion { get; set; }
    }
}