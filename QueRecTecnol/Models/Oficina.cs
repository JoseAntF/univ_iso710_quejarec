﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QueRecTecnol.Models
{
    public class Oficina
    {
        [Key]
        public int OficinaID { get; set; }

        [Required]
        [Display(Name = "Nombre de oficina")]
        public string Descripcion { get; set; }

        [Required]
        [Display(Name = "Direccion de oficina")]
        public string Direccion { get; set; }
    }
}