﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace QueRecTecnol.Models
{
    public class Cliente
    {
        [Key]
        public int ClienteID { get; set; }
        [Required]
        public string Nombre { get; set; }

        [Required]
        public int TipoIdentificacionID { get; set; }

        [ForeignKey("TipoIdentificacionID")]
        [Display(Name = "Tipo de identificación")]
        virtual public TipoIdentificacion TipoIdentificacion { get; set; }

        [Required]
        [Display(Name = "Identificación")]
        public string Identificacion { get; set; }
        [Required]
        [Display(Name = "Teléfono")]
        public string Telefono { get; set; }
        [Required]
        [Display(Name = "Correo Electrónico")]
        [EmailAddress]
        public string Correo { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime FechaCreacion { get; set; }

        [Required]
        public int EstadoClienteID { get; set; }

        [ForeignKey("EstadoClienteID")]
        [Display(Name = "Estado")]
        virtual public EstadoCliente EstadoCliente { get; set; }

        [Required]
        public int OficinaID { get; set; }

        [ForeignKey("OficinaID")]
        [Display(Name = "Oficina")]
        virtual public Oficina Oficina { get; set; }

        [Required]
        public int EmpleadoID { get; set; }

        [ForeignKey("EmpleadoID")]
        [Display(Name = "Oficial Encargado")]
        virtual public Empleado Oficial { get; set; }

        [Required]
        [Display(Name = "Provincia")]
        public string Provincia { get; set; }

        [Required]
        [Display(Name = "Sector")]
        public string Sector { get; set; }

        [Required]
        [Display(Name = "Municipio")]
        public string Municipio { get; set; }

        [Required]
        [Display(Name = "Barrio / Ciudad")]
        public string Barrio { get; set; }

        [Required]
        [Display(Name = "Dirección 1")]
        public string Direccion1 { get; set; }

        
        [Display(Name = "Dirección 2")]
        public string Direccion2 { get; set; }

        [Required]
        public int PaisID { get; set; }

        [ForeignKey("PaisID")]
        [Display(Name = "País")]
        virtual public Pais Pais { get; set; }

        [Display(Name = "Productos comprados")]
        virtual public ICollection<Producto> Productos { get; set; }
    }
}