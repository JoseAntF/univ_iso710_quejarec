﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QueRecTecnol.Models
{
    public class Empleado
    {
        [Key]
        [Display(Name = "Empleado")]
        public int EmpleadoID { get; set; }
        [Required]
        public string Nombre { get; set; }

        [Display(Name = "Departamento")]
        public int DepartamentoID { get; set; }

        [ForeignKey("DepartamentoID")]
        virtual public Departamento Departamento { get; set; }
    }
}