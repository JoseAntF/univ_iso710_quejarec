﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QueRecTecnol.Models
{
    public class TipoQuejaReclamo
    {
        [Key]
        public int TipoQueRec_ID { get; set; }

        [Required]
        [Display(Name = "Descripción")]
        public string Descripcion { get; set; }

        [Display(Name = "¿Es para reclamos?")]
        public bool EsParaReclamo { get; set; }

        [NotMapped]
        public string NombrePublico { get
            { return $"{(EsParaReclamo ? "Reclamo" : "Queja")} - {Descripcion}"; } }
    }
}