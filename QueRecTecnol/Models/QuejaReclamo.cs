﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace QueRecTecnol.Models
{

    public abstract class QuejaReclamoBase
    {
        [Key]
        public int QuejaRecID { get; set; }

        [Required]
        [Display(Name = "Departamento")]
        public int DepartamentoID { get; set; }

        [ForeignKey("DepartamentoID")]
        virtual public Departamento Departamento { get; set; }

        [Required]
        [Display(Name = "Tipo")]
        public int TipoQueRec_ID { get; set; }

        [ForeignKey("TipoQueRec_ID")]
        virtual public TipoQuejaReclamo Tipo { get; set; }

        [Required]
        [Display(Name = "Cliente levantador")]
        public int ClienteID { get; set; }

        [ForeignKey("ClienteID")]
        virtual public Cliente Cliente { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        [Required]
        public string Detalle { get; set; }

        [Required]
        
        public int EstadoQueRec_ID { get; set; }

        [Display(Name = "Estado")]
        virtual public EstadoQuejaReclamo Estado { get; set; }
    }

    [Table("Quejas")]
    public class Queja : QuejaReclamoBase
    {
        // Ya nada, no hay tipos específicos, eso se maneja de otra manera.
    }

    [Table("Reclamos")]
    public class Reclamacion : QuejaReclamoBase
    {

        [Required]
        [Display(Name = "Producto afectado")]
        public int ProductoID { get; set; }

        [ForeignKey("ProductoID")]
        virtual public Producto Producto { get; set; }

    }
}