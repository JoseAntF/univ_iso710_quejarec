﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QueRecTecnol.Models
{
    public class EstadoCliente
    {
        [Key]
        public int EstadoClienteID { get; set; }

        [Required]
        [Display(Name = "Nombre de Estado")]
        public string Descripcion { get; set; }
    }
}