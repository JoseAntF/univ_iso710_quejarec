﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace QueRecTecnol.Models
{
    public class Pais
    {
        [Key]
        public int PaisID { get; set; }

        [Required]
        public string Nombre { get; set; }
    }
}