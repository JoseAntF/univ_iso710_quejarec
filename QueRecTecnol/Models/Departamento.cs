﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace QueRecTecnol.Models
{
    public class Departamento
    {
        [Key]
        [Display(Name = "Departamento")]
        public int DepartamentoID { get; set; }
        [Required]
        public string Nombre { get; set; }
    }
}