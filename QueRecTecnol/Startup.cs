﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using QueRecTecnol.Models;
using System;
using System.Threading.Tasks;

[assembly: OwinStartupAttribute(typeof(QueRecTecnol.Startup))]
namespace QueRecTecnol
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
