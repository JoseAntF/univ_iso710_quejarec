﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QueRecTecnol.Models;

namespace QueRecTecnol.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EstadosQuejaReclamoController : Controller
    {
        private CompDbContext db = new CompDbContext();

        // GET: EstadosQuejaReclamo
        public ActionResult Index()
        {
            return View(db.EstadoQuejaReclamo.ToList());
        }

        // GET: EstadosQuejaReclamo/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoQuejaReclamo estadoQuejaReclamo = db.EstadoQuejaReclamo.Find(id);
            if (estadoQuejaReclamo == null)
            {
                return HttpNotFound();
            }
            return View(estadoQuejaReclamo);
        }

        // GET: EstadosQuejaReclamo/Create
        public ActionResult Create()
        {
            return View(new EstadoQuejaReclamo());
        }

        // POST: EstadosQuejaReclamo/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EstadoQueRec_ID,Descripcion,CierraTiquet")] EstadoQuejaReclamo estadoQuejaReclamo)
        {
            if (ModelState.IsValid)
            {
                db.EstadoQuejaReclamo.Add(estadoQuejaReclamo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estadoQuejaReclamo);
        }

        // GET: EstadosQuejaReclamo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoQuejaReclamo estadoQuejaReclamo = db.EstadoQuejaReclamo.Find(id);
            if (estadoQuejaReclamo == null)
            {
                return HttpNotFound();
            }
            return View(estadoQuejaReclamo);
        }

        // POST: EstadosQuejaReclamo/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EstadoQueRec_ID,Descripcion,CierraTiquet")] EstadoQuejaReclamo estadoQuejaReclamo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estadoQuejaReclamo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estadoQuejaReclamo);
        }

        // GET: EstadosQuejaReclamo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoQuejaReclamo estadoQuejaReclamo = db.EstadoQuejaReclamo.Find(id);
            if (estadoQuejaReclamo == null)
            {
                return HttpNotFound();
            }
            return View(estadoQuejaReclamo);
        }

        // POST: EstadosQuejaReclamo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EstadoQuejaReclamo estadoQuejaReclamo = db.EstadoQuejaReclamo.Find(id);
            db.EstadoQuejaReclamo.Remove(estadoQuejaReclamo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
