﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QueRecTecnol.Models;

namespace QueRecTecnol.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TiposQuejaReclamoController : Controller
    {
        private CompDbContext db = new CompDbContext();

        // GET: TiposQuejaReclamo
        public ActionResult Index()
        {
            return View(db.TipoQuejasReclamos.ToList());
        }

        // GET: TiposQuejaReclamo/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoQuejaReclamo tipoQuejaReclamo = db.TipoQuejasReclamos.Find(id);
            if (tipoQuejaReclamo == null)
            {
                return HttpNotFound();
            }
            return View(tipoQuejaReclamo);
        }

        // GET: TiposQuejaReclamo/Create
        public ActionResult Create()
        {
            return View(new TipoQuejaReclamo());
        }

        // POST: TiposQuejaReclamo/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TipoQueRec_ID,Descripcion,EsParaReclamo")] TipoQuejaReclamo tipoQuejaReclamo)
        {
            if (ModelState.IsValid)
            {
                db.TipoQuejasReclamos.Add(tipoQuejaReclamo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoQuejaReclamo);
        }

        // GET: TiposQuejaReclamo/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoQuejaReclamo tipoQuejaReclamo = db.TipoQuejasReclamos.Find(id);
            if (tipoQuejaReclamo == null)
            {
                return HttpNotFound();
            }
            return View(tipoQuejaReclamo);
        }

        // POST: TiposQuejaReclamo/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TipoQueRec_ID,Descripcion,EsParaReclamo")] TipoQuejaReclamo tipoQuejaReclamo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoQuejaReclamo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoQuejaReclamo);
        }

        // GET: TiposQuejaReclamo/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoQuejaReclamo tipoQuejaReclamo = db.TipoQuejasReclamos.Find(id);
            if (tipoQuejaReclamo == null)
            {
                return HttpNotFound();
            }
            return View(tipoQuejaReclamo);
        }

        // POST: TiposQuejaReclamo/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoQuejaReclamo tipoQuejaReclamo = db.TipoQuejasReclamos.Find(id);
            db.TipoQuejasReclamos.Remove(tipoQuejaReclamo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
