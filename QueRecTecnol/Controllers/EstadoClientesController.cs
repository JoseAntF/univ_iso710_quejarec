﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QueRecTecnol.Models;

namespace QueRecTecnol.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EstadoClientesController : Controller
    {
        private CompDbContext db = new CompDbContext();

        // GET: EstadoClientes
        public ActionResult Index()
        {
            return View(db.EstadosClientes.ToList());
        }

        // GET: EstadoClientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoCliente estadoCliente = db.EstadosClientes.Find(id);
            if (estadoCliente == null)
            {
                return HttpNotFound();
            }
            return View(estadoCliente);
        }

        // GET: EstadoClientes/Create
        public ActionResult Create()
        {
            return View(new EstadoCliente());
        }

        // POST: EstadoClientes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EstadoClienteID,Descripcion")] EstadoCliente estadoCliente)
        {
            if (ModelState.IsValid)
            {
                db.EstadosClientes.Add(estadoCliente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(estadoCliente);
        }

        // GET: EstadoClientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoCliente estadoCliente = db.EstadosClientes.Find(id);
            if (estadoCliente == null)
            {
                return HttpNotFound();
            }
            return View(estadoCliente);
        }

        // POST: EstadoClientes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EstadoClienteID,Descripcion")] EstadoCliente estadoCliente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(estadoCliente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(estadoCliente);
        }

        // GET: EstadoClientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EstadoCliente estadoCliente = db.EstadosClientes.Find(id);
            if (estadoCliente == null)
            {
                return HttpNotFound();
            }
            return View(estadoCliente);
        }

        // POST: EstadoClientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EstadoCliente estadoCliente = db.EstadosClientes.Find(id);
            db.EstadosClientes.Remove(estadoCliente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
