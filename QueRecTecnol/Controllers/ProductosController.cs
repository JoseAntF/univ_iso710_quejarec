﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using QueRecTecnol.Models;

namespace QueRecTecnol.Controllers
{
    public class ProductosController : Controller
    {
        private CompDbContext db = new CompDbContext();

        // GET: Productos
        public ActionResult Index()
        {
            return View(db.Productos.ToList());
        }

        // Relacionado a Reporte //

        struct ProductoReportable
        {
            public string Nombre { get; set; }
            public decimal Precio { get; set; }
            public string Estado { get; set; }
        }

        ProductoReportable HacerPReportable(Producto c)
        {
            return new ProductoReportable
            {
                Nombre = c.Nombre,
                Precio = (decimal)c.Precio,
                Estado = Enum.GetName(typeof(EstatusProducto), c.Estado)
            };
        }

        // GET: Productos/Reporte
        [Authorize(Roles = "Admin")]
        public ActionResult Reporte()
        {
            StringBuilder csv = new StringBuilder();
            var productos = db.Productos.AsEnumerable().Select(r => HacerPReportable(r));

            csv.AppendLine("Nombre;Precio;Estado");

            foreach (ProductoReportable cR in productos)
            {
                csv.AppendFormat("\"{0}\";\"{1}\";\"=\"\"{2}\"\"\"\n",
                    cR.Nombre, cR.Precio, cR.Estado);
            }
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = "reporteProductos.csv",
                Inline = false
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            Response.Charset = "utf-8";
            var data = Encoding.UTF8.GetBytes(csv.ToString());
            var result = Encoding.UTF8.GetPreamble().Concat(data).ToArray();
            return File(result, "text/csv");
        }

        // GET: Productos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // GET: Productos/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View(new Producto());
        }

        // POST: Productos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Create([Bind(Include = "ProductoID,Nombre,Precio,Estado")] Producto producto)
        {
            if (ModelState.IsValid)
            {
                db.Productos.Add(producto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(producto);
        }

        // GET: Productos/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: Productos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "ProductoID,Nombre,Precio,Estado")] Producto producto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(producto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(producto);
        }

        // GET: Productos/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Productos.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: Productos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Producto producto = db.Productos.Find(id);
            db.Productos.Remove(producto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
