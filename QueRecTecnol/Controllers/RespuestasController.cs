﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QueRecTecnol.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Diagnostics;
using System.Text;

namespace QueRecTecnol.Controllers
{
    [Authorize]
    public class RespuestasController : Controller
    {
        private CompDbContext db = new CompDbContext();


        // Cosas de OWIN
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Respuestas
        public ActionResult Index()
        {
            var respuestas = db.Respuestas.Include(r => r.Departamento).Include(r => r.Empleado).ToList();

            // Si es admin tiene acceso a todo sin importar si tiene objeto asignado
            if (!User.IsInRole("Admin"))
            {
                // Si tiene alguno de estos roles pues tiene que tener un objeto asignado OBLIGATORIAMENTE
                if (User.IsInRole("Empleado"))
                {
                    int eID = UserManager.FindById(User.Identity.GetUserId()).EmpleadoAsignado.EmpleadoID;
                    respuestas = respuestas.Where(x => x.EmpleadoID
                                == eID).ToList();
                }
                else if (User.IsInRole("Cliente"))
                {
                    int clID = UserManager.FindById(User.Identity.GetUserId()).ClienteAsignado.ClienteID;
                    respuestas = respuestas.Where(
                        (x) =>
                        {
                            Debug.WriteLine(x.QuejaRecID);
                            Debug.WriteLine(db.QuejasReclamos.Find(x.QuejaRecID).ClienteID);
                            return clID == db.QuejasReclamos.Find(x.QuejaRecID).ClienteID;
                        }).ToList();

                }
            }
            return View(respuestas);
        }


        // Relacionado a Reporte //

        struct RespReportable
        {
            public string RespondeA { get; set; }
            public string Encargado { get; set; }
            public string Departamento { get; set; }
            public DateTime Fecha { get; set; }
            public string Detalle { get; set; }
            public string Inconformidad { get; set; }
            public int? Valoración { get; set; }
        }

        RespReportable HacerRReportable(Respuesta c)
        {
            return new RespReportable
            {
                RespondeA = $"-> #{c.QuejaRecID}",
                Departamento = c.Departamento.Nombre,
                Encargado = c.Empleado.Nombre,
                Inconformidad = (string.IsNullOrEmpty(c.Inconformidad) ? c.Inconformidad : "<Nada>"),
                Fecha = c.FechaRespuesta,
                Detalle = c.Detalle,
                Valoración = c.Valoracion,
            };
        }

        // GET: Respuestas/Reporte
        [Authorize(Roles = "Admin")]
        public ActionResult Reporte()
        {
            StringBuilder csv = new StringBuilder();
            var respuestas = db.Respuestas.Include(r => r.Departamento).Include(r => r.Empleado)
                .AsEnumerable().Select(r => HacerRReportable(r));

            csv.AppendLine("Responde A...;Departamento;Encargado;Fecha;Detalle;Inconformidad;Valoración?");

            foreach (RespReportable cR in respuestas)
            {
                csv.AppendFormat("\"{0}\";\"{1}\";\"=\"\"{2}\"\"\";\"=\"\"{3}\"\"\";\"{4}\";\"{5}\";\"{6}\"\n",
                    cR.RespondeA, cR.Departamento, cR.Encargado,
                    cR.Fecha, cR.Detalle, cR.Inconformidad, cR.Valoración);
            }
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = "reporteRespuestas.csv",
                Inline = false
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            Response.Charset = "utf-8";
            var data = Encoding.UTF8.GetBytes(csv.ToString());
            var result = Encoding.UTF8.GetPreamble().Concat(data).ToArray();
            return File(result, "text/csv");
        }

        // GET: Respuestas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Respuesta respuesta = db.Respuestas.Find(id);
            if (respuesta == null)
            {
                return HttpNotFound();
            }
            return View(respuesta);
        }

        // GET: Respuestas/Create
        [Authorize(Roles = "Admin,Empleado")]
        public ActionResult Create()
        {
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "Nombre");
            ViewBag.EmpleadoID = new SelectList(db.Empleados, "EmpleadoID", "Nombre");
            ViewBag.QuejaRecID = new SelectList(db.QuejasReclamos, "QuejaRecID", "Detalle");
            ViewBag.EstadoQueRec_ID = new SelectList(db.EstadoQuejaReclamo, "EstadoQueRec_ID", "Descripcion");
            Respuesta resp = new Respuesta();
            resp.FechaRespuesta = DateTime.Now;
            return View(resp);
        }

        // POST: Respuestas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Empleado")]
        public ActionResult Create([Bind(Include = "RespuestaID,EmpleadoID,DepartamentoID,FechaRespuesta,Detalle,QuejaRecID")] Respuesta respuesta, int EstadoQueRec_ID)
        {
            // Si no es admin y tiene un objeto empleado asignado
            if (!User.IsInRole("Admin") && UserManager.FindById(User.Identity.GetUserId()).EmpleadoAsignado != null)
            {
                respuesta.EmpleadoID = UserManager.FindById(User.Identity.GetUserId()).EmpleadoAsignado.EmpleadoID;
                respuesta.DepartamentoID = UserManager.FindById(User.Identity.GetUserId()).EmpleadoAsignado.DepartamentoID;
            }

            if (ModelState.IsValid)
            {
                db.QuejasReclamos.Find(respuesta.QuejaRecID).EstadoQueRec_ID = EstadoQueRec_ID;
                db.Respuestas.Add(respuesta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "Nombre", respuesta.DepartamentoID);
            ViewBag.EmpleadoID = new SelectList(db.Empleados, "EmpleadoID", "Nombre", respuesta.EmpleadoID);
            ViewBag.EstadoQueRec_ID = new SelectList(db.EstadoQuejaReclamo, "EstadoQueRec_ID", "Descripcion",
                                        db.QuejasReclamos.Find(respuesta.QuejaRecID).Estado.EstadoQueRec_ID);
            return View(respuesta);
        }

        // GET: Respuestas/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            // Edición es para Admin, Rectificación es para Empleado
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Respuesta respuesta = db.Respuestas.Find(id);
            if (respuesta == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "Nombre", respuesta.DepartamentoID);
            ViewBag.EmpleadoID = new SelectList(db.Empleados, "EmpleadoID", "Nombre", respuesta.EmpleadoID);
            return View(respuesta);
        }

        // POST: Respuestas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "RespuestaID,EmpleadoID,DepartamentoID,FechaRespuesta,QuejaRecID,Detalle")] Respuesta respuesta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(respuesta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "DepartamentoID", respuesta.DepartamentoID);
            ViewBag.EmpleadoID = new SelectList(db.Empleados, "EmpleadoID", "Nombre", respuesta.EmpleadoID);
            return View(respuesta);
        }

        // GET: Respuestas/Rectificar/5
        [Authorize(Roles = "Admin,Empleado")]
        public ActionResult Rectificar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Respuesta respuesta = db.Respuestas.Find(id);
            if (respuesta == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "Nombre", respuesta.DepartamentoID);
            ViewBag.EmpleadoID = new SelectList(db.Empleados, "EmpleadoID", "Nombre", respuesta.EmpleadoID);
            ViewBag.EstadoQueRec_ID = new SelectList(db.EstadoQuejaReclamo, "EstadoQueRec_ID", "Descripcion",
                                        db.QuejasReclamos.Find(respuesta.QuejaRecID).Estado.EstadoQueRec_ID);
            return View(respuesta);
        }

        // POST: Respuestas/Rectificar/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Empleado")]
        public ActionResult Rectificar([Bind(Include = "RespuestaID,EmpleadoID,DepartamentoID,FechaRespuesta,QuejaRecID,Detalle,Valoracion")] Respuesta respuesta, int EstadoQueRec_ID, bool ReiniciarInconformidad)
        {
            if (ModelState.IsValid)
            {
                if (ReiniciarInconformidad)
                {
                    respuesta.Inconformidad = "";
                    respuesta.Valoracion = null;
                }
                db.QuejasReclamos.Find(respuesta.QuejaRecID).EstadoQueRec_ID = EstadoQueRec_ID;
                db.Entry(respuesta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "DepartamentoID", respuesta.DepartamentoID);
            ViewBag.EmpleadoID = new SelectList(db.Empleados, "EmpleadoID", "Nombre", respuesta.EmpleadoID);
            ViewBag.EstadoQueRec_ID = new SelectList(db.EstadoQuejaReclamo, "EstadoQueRec_ID", "Descripcion",
                                        db.QuejasReclamos.Find(respuesta.QuejaRecID).Estado.EstadoQueRec_ID);
            return View(respuesta);
        }

        // GET: Respuestas/Valorar/5
        [Authorize(Roles = "Admin,Cliente")]
        public ActionResult Valorar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Respuesta respuesta = db.Respuestas.Find(id);
            if (respuesta == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "Nombre", respuesta.DepartamentoID);
            ViewBag.EmpleadoID = new SelectList(db.Empleados, "EmpleadoID", "Nombre", respuesta.EmpleadoID);
            ViewBag.EstadoQueRec_ID = new SelectList(db.EstadoQuejaReclamo, "EstadoQueRec_ID", "Descripcion",
                                        db.QuejasReclamos.Find(respuesta.QuejaRecID).Estado.EstadoQueRec_ID);
            return View(respuesta);
        }

        // POST: Respuestas/Valorar/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Cliente")]
        public ActionResult Valorar([Bind(Include = "RespuestaID,EmpleadoID,DepartamentoID,FechaRespuesta,QuejaRecID,Detalle")] Respuesta respuesta, int? valor)
        {
            if (valor == null)
            {
                ModelState.AddModelError("valor", "Seleccione un valor");
            }
            if (ModelState.IsValid)
            {
                respuesta.Valoracion = valor;
                db.Entry(respuesta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "DepartamentoID", respuesta.DepartamentoID);
            ViewBag.EmpleadoID = new SelectList(db.Empleados, "EmpleadoID", "Nombre", respuesta.EmpleadoID);
            ViewBag.EstadoQueRec_ID = new SelectList(db.EstadoQuejaReclamo, "EstadoQueRec_ID", "Descripcion",
                                        db.QuejasReclamos.Find(respuesta.QuejaRecID).Estado.EstadoQueRec_ID);
            return View(respuesta);
        }

        // GET: Respuestas/Edit/5
        [Authorize(Roles = "Admin,Cliente")]
        public ActionResult Inconformidad(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Respuesta respuesta = db.Respuestas.Find(id);
            if (respuesta == null)
            {
                return HttpNotFound();
            }
            if (!User.IsInRole("Admin") && UserManager.FindById(User.Identity.GetUserId()).ClienteAsignado?.ClienteID
                != respuesta.QuejaReclamo.ClienteID)
            {
                return View("Unauthorized");
            }
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "Nombre", respuesta.DepartamentoID);
            ViewBag.EmpleadoID = new SelectList(db.Empleados, "EmpleadoID", "Nombre", respuesta.EmpleadoID);
            return View(respuesta);
        }

        // POST: Inconformidad/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Cliente")]
        public ActionResult Inconformidad([Bind(Include = "RespuestaID,EmpleadoID,DepartamentoID,FechaRespuesta,QuejaRecID,Detalle,Inconformidad")] Respuesta respuesta)
        {
            if (ModelState.IsValid)
            {
                db.QuejasReclamos.Find(respuesta.QuejaRecID).Estado = db.EstadoQuejaReclamo.FirstOrDefault();
                db.Entry(respuesta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "DepartamentoID", respuesta.DepartamentoID);
            ViewBag.EmpleadoID = new SelectList(db.Empleados, "EmpleadoID", "Nombre", respuesta.EmpleadoID);
            return View(respuesta);
        }

        public ActionResult Estadistica()
        {
            int[] valoraciones = new int[6];
            float cuentaTotal = db.Respuestas.Count();
            valoraciones[0] = db.Respuestas.Where(x => x.Valoracion == 0).Count();
            valoraciones[1] = db.Respuestas.Where(x => x.Valoracion == 1).Count();
            valoraciones[2] = db.Respuestas.Where(x => x.Valoracion == 2).Count();
            valoraciones[3] = db.Respuestas.Where(x => x.Valoracion == 3).Count();
            valoraciones[4] = db.Respuestas.Where(x => x.Valoracion == 4).Count();
            valoraciones[5] = db.Respuestas.Where(x => x.Valoracion == 5).Count();

            ViewBag.Valoraciones = valoraciones;
            ViewBag.CuentaTotal = cuentaTotal;
            return View();
        }

        public ActionResult ExportarEstadistica()
        {
            string[] puntuaciones = new string[]
            {
              "PÉSIMO",
              "MALO",
              "EH...",
              "DECENTE",
              "BUENO",
              "GENIAL"
            };
            float rtotales = db.Respuestas.Count();
            StringBuilder csv = new StringBuilder();
            csv.AppendLine($"\" \";Frecuencia;Pct. Total");
            int sval = db.Respuestas.Where(x => x.Valoracion == null).Count();
            csv.AppendLine($"Sin valorar;{sval};{sval / rtotales * 100}%");
            for (int i = 0; i < 6; i++)
            {
                int cuenta = db.Respuestas.Where(x => x.Valoracion == i).Count();
                csv.AppendLine($"{puntuaciones[i]};{cuenta};{cuenta/rtotales * 100}%");
            }
            csv.AppendFormat($"Respuestas totales;{rtotales}");

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = "estadisticaRespuestas.csv",
                Inline = false
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            Response.Charset = "utf-8";
            var data = Encoding.UTF8.GetBytes(csv.ToString());
            var result = Encoding.UTF8.GetPreamble().Concat(data).ToArray();
            return File(result, "text/csv");
        }

        // GET: Respuestas/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Respuesta respuesta = db.Respuestas.Find(id);
            if (respuesta == null)
            {
                return HttpNotFound();
            }
            return View(respuesta);
        }

        // POST: Respuestas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Respuesta respuesta = db.Respuestas.Find(id);
            db.Respuestas.Remove(respuesta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
