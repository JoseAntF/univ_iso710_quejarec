﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using QueRecTecnol.Models;

namespace QueRecTecnol.Controllers
{
    [Authorize]
    public class QuejasController : Controller
    {
        private CompDbContext db = new CompDbContext();

        // Cosas de OWIN
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Quejas
        public ActionResult Index()
        {
            var quejaReclamoBases = db.QuejasReclamos.Include(q => q.Cliente).Include(q => q.Departamento);
            return View(quejaReclamoBases.ToList());
        }

        // GET: Quejas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Queja queja = db.QuejasReclamos.OfType<Queja>().Where(x => x.QuejaRecID == id).First();
            queja.Cliente = db.Clientes.Find(queja.ClienteID);
            queja.Departamento = db.Departamentos.Find(queja.DepartamentoID);
            if (queja == null)
            {
                return HttpNotFound();
            }
            return View(queja);
        }

        // GET: Quejas/Create
        public ActionResult Create()
        {
            ViewBag.ClienteID = new SelectList(db.Clientes, "ClienteID", "Nombre");
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "Nombre");
            ViewBag.TipoQueRec_ID = new SelectList(db.TipoQuejasReclamos.Where(x => !x.EsParaReclamo), "TipoQueRec_ID", "Descripcion");
            ViewBag.EstadoQueRec_ID = new SelectList(db.EstadoQuejaReclamo, "EstadoQueRec_ID", "Descripcion");
            Queja q = new Queja
            {
                Fecha = DateTime.Now
            };
            return View(q);
        }

        // POST: Quejas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "QuejaRecID,DepartamentoID,ClienteID,Fecha,Detalle,TipoQueRec_ID,EstadoQueRec_ID")] Queja queja)
        {
            if (ModelState.IsValid)
            {
                queja.ClienteID = UserManager.FindById(User.Identity.GetUserId()).ClienteAsignado.ClienteID;
                queja.EstadoQueRec_ID = db.EstadoQuejaReclamo.FirstOrDefault().EstadoQueRec_ID;
                db.QuejasReclamos.Add(queja);
                db.SaveChanges();
                return RedirectToAction("Index", "QuejasReclamos");
            }

            ViewBag.ClienteID = new SelectList(db.Clientes, "ClienteID", "Nombre", queja.ClienteID);
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "Nombre", queja.DepartamentoID);
            ViewBag.TipoQueRec_ID = new SelectList(db.TipoQuejasReclamos.Where(x => !x.EsParaReclamo), "TipoQueRec_ID", "Descripcion", queja.TipoQueRec_ID);
            ViewBag.EstadoQueRec_ID = new SelectList(db.EstadoQuejaReclamo, "EstadoQueRec_ID", "Descripcion", queja.EstadoQueRec_ID);
            return View(queja);
        }

        // GET: Quejas/Edit/5

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Queja queja = db.QuejasReclamos.OfType<Queja>().Where(x => x.QuejaRecID == id).First();
            if (queja == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClienteID = new SelectList(db.Clientes, "ClienteID", "Nombre", queja.ClienteID);
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "Nombre", queja.DepartamentoID);
            ViewBag.TipoQueRec_ID = new SelectList(db.TipoQuejasReclamos.Where(x => !x.EsParaReclamo), "TipoQueRec_ID", "Descripcion", queja.TipoQueRec_ID);
            ViewBag.EstadoQueRec_ID = new SelectList(db.EstadoQuejaReclamo, "EstadoQueRec_ID", "Descripcion", queja.EstadoQueRec_ID);
            return View(queja);
        }

        // POST: Quejas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit([Bind(Include = "QuejaRecID,DepartamentoID,ClienteID,Fecha,Detalle,TipoQueRec_ID,EstadoQueRec_ID")] Queja queja)
        {
            if (ModelState.IsValid)
            {
                db.Entry(queja).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "QuejasReclamos");
            }
            ViewBag.ClienteID = new SelectList(db.Clientes, "ClienteID", "Nombre", queja.ClienteID);
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "Nombre", queja.DepartamentoID);
            ViewBag.TipoQueRec_ID = new SelectList(db.TipoQuejasReclamos.Where(x => !x.EsParaReclamo), "TipoQueRec_ID", "Descripcion", queja.TipoQueRec_ID);
            ViewBag.EstadoQueRec_ID = new SelectList(db.EstadoQuejaReclamo, "EstadoQueRec_ID", "Descripcion", queja.EstadoQueRec_ID);
            return View(queja);
        }

        // GET: Quejas/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Queja queja = db.QuejasReclamos.OfType<Queja>().Where(x => x.QuejaRecID == id).First();
            if (queja == null)
            {
                return HttpNotFound();
            }
            return View(queja);
        }

        // POST: Quejas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Queja queja = db.QuejasReclamos.OfType<Queja>().Where(x => x.QuejaRecID == id).First();
            db.QuejasReclamos.Remove(queja);
            db.SaveChanges();
            return RedirectToAction("Index", "QuejasReclamos");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
