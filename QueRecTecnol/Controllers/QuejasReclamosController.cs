﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using QueRecTecnol.Models;

namespace QueRecTecnol.Controllers
{
    [Authorize]
    public class QuejasReclamosController : Controller
    {
        private CompDbContext db = new CompDbContext();

        // Cosas de OWIN
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: QuejasReclamos
        public ActionResult Index(string e, string query = null)
        {
            var quejasReclamos = db.QuejasReclamos.Include(q => q.Cliente).Include(q => q.Departamento);
            switch (e)
            {
                case null:
                default:
                    break;
                case "0":
                    quejasReclamos = quejasReclamos.Where(x => !x.Estado.CierraTiquet);
                    break;
                case "1":
                    quejasReclamos = quejasReclamos.Where(x => x.Estado.CierraTiquet);
                    break;
            }

            // Si es admin tiene acceso a todo sin importar si tiene objeto asignado
            if (!User.IsInRole("Admin"))
            {
                // Si tiene alguno de estos roles pues tiene que tener un objeto asignado OBLIGATORIAMENTE
                 if (User.IsInRole("Cliente"))
                {
                    int clID = UserManager.FindById(User.Identity.GetUserId()).ClienteAsignado.ClienteID;
                    quejasReclamos = quejasReclamos.Where(x => x.ClienteID
                                == clID);

                }
            }

            if (!string.IsNullOrEmpty(query))
            {
                quejasReclamos = quejasReclamos.Where(x => x.Cliente.Nombre.Contains(query));
            }
            return View(quejasReclamos.ToList());
        }

        // Relacionado a Reporte //

        struct QueRecReportable
        {
            public string ID { get; set; }
            public string Departamento { get; set; }
            public string Tipo { get; set; }
            public string Cliente { get; set; }
            public DateTime Fecha { get; set; }
            public string Detalle { get; set; }
            public string Estado { get; set; }
            public string Producto { get; set; }
        }

        QueRecReportable HacerQRReportable(QuejaReclamoBase c)
        {
            return new QueRecReportable
            {
                ID = c.QuejaRecID.ToString(),
                Departamento = c.Departamento.Nombre,
                Tipo = c.Tipo.NombrePublico,
                Cliente = c.Cliente.Nombre,
                Fecha = c.Fecha,
                Detalle = c.Detalle,
                Estado = c.Estado.Descripcion,
                Producto = (c is Reclamacion reclamacion ? reclamacion.Producto.Nombre : "N/A")
            };
        }

        // GET: QuejasReclamos/Reporte
        [Authorize(Roles = "Admin")]
        public ActionResult Reporte(string e, string query = null)
        {
            StringBuilder csv = new StringBuilder();
            var quejasReclamos = db.QuejasReclamos.Include(q => q.Cliente).Include(q => q.Departamento);
            switch (e)
            {
                case null:
                default:
                    break;
                case "0":
                    quejasReclamos = quejasReclamos.Where(x => !x.Estado.CierraTiquet);
                    break;
                case "1":
                    quejasReclamos = quejasReclamos.Where(x => x.Estado.CierraTiquet);
                    break;
            }
            if (!string.IsNullOrEmpty(query))
            {
                quejasReclamos = quejasReclamos.Where(x => x.Cliente.Nombre.Contains(query));
            }
            var qR = quejasReclamos.AsEnumerable().Select(q => HacerQRReportable(q));

            csv.AppendLine("ID;Departamento;Tipo;Cliente;Fecha;Detalle;Estado;Producto?");

            foreach (QueRecReportable cR in qR)
            {
                csv.AppendFormat("\"{0}\";\"{1}\";\"=\"\"{2}\"\"\";\"=\"\"{3}\"\"\";\"{4}\";\"{5}\";\"{6}\";\"{7}\"\n",
                    cR.ID, cR.Departamento, cR.Tipo,
                    cR.Cliente, cR.Fecha, cR.Detalle, cR.Estado,
                    cR.Producto);
            }
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = "reporteQuejasReclamos.csv",
                Inline = false
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            Response.Charset = "utf-8";
            var data = Encoding.UTF8.GetBytes(csv.ToString());
            var result = Encoding.UTF8.GetPreamble().Concat(data).ToArray();
            return File(result, "text/csv");
        }

        // GET: QuejasReclamos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QuejaReclamoBase quejaReclamoBase = db.QuejasReclamos.Find(id);
            quejaReclamoBase.Cliente = db.Clientes.Find(quejaReclamoBase.ClienteID);
            quejaReclamoBase.Departamento = db.Departamentos.Find(quejaReclamoBase.DepartamentoID);
            if (quejaReclamoBase == null)
            {
                return HttpNotFound();
            }
            return View(quejaReclamoBase);
        }

        // POST: QuejasReclamos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QuejaReclamoBase quejaReclamoBase = db.QuejasReclamos.Find(id);
            db.QuejasReclamos.Remove(quejaReclamoBase);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
