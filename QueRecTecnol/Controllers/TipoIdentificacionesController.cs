﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using QueRecTecnol.Models;

namespace QueRecTecnol.Controllers
{
    [Authorize(Roles = "Admin")]
    public class TipoIdentificacionesController : Controller
    {
        private CompDbContext db = new CompDbContext();

        // GET: TipoIdentificaciones
        public ActionResult Index()
        {
            return View(db.TipoIdentificacion.ToList());
        }

        // GET: TipoIdentificaciones/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoIdentificacion tipoIdentificacion = db.TipoIdentificacion.Find(id);
            if (tipoIdentificacion == null)
            {
                return HttpNotFound();
            }
            return View(tipoIdentificacion);
        }

        // GET: TipoIdentificaciones/Create
        public ActionResult Create()
        {
            return View(new TipoIdentificacion());
        }

        // POST: TipoIdentificaciones/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TipoIdentificacionID,Descripcion")] TipoIdentificacion tipoIdentificacion)
        {
            if (ModelState.IsValid)
            {
                db.TipoIdentificacion.Add(tipoIdentificacion);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipoIdentificacion);
        }

        // GET: TipoIdentificaciones/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoIdentificacion tipoIdentificacion = db.TipoIdentificacion.Find(id);
            if (tipoIdentificacion == null)
            {
                return HttpNotFound();
            }
            return View(tipoIdentificacion);
        }

        // POST: TipoIdentificaciones/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TipoIdentificacionID,Descripcion")] TipoIdentificacion tipoIdentificacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoIdentificacion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipoIdentificacion);
        }

        // GET: TipoIdentificaciones/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoIdentificacion tipoIdentificacion = db.TipoIdentificacion.Find(id);
            if (tipoIdentificacion == null)
            {
                return HttpNotFound();
            }
            return View(tipoIdentificacion);
        }

        // POST: TipoIdentificaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipoIdentificacion tipoIdentificacion = db.TipoIdentificacion.Find(id);
            db.TipoIdentificacion.Remove(tipoIdentificacion);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
