﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using QueRecTecnol.Models;

namespace QueRecTecnol.Controllers
{
    [Authorize]
    public class ReclamacionesController : Controller
    {
        private CompDbContext db = new CompDbContext();

        // Cosas de OWIN
        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Reclamaciones
        public ActionResult Index()
        {
            var quejaReclamoBases = db.QuejasReclamos.OfType<Reclamacion>().Include(r => r.Cliente).Include(r => r.Departamento).Include(r => r.Producto);
            return View(quejaReclamoBases.ToList());
        }

        // GET: Reclamaciones/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reclamacion reclamacion = db.QuejasReclamos.OfType<Reclamacion>().Where(x => x.QuejaRecID == id).First();
            if (reclamacion == null)
            {
                return HttpNotFound();
            }
            return View(reclamacion);
        }

        // GET: Reclamaciones/Create
        public ActionResult Create()
        {
            ViewBag.ClienteID = new SelectList(db.Clientes, "ClienteID", "Nombre");
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "Nombre");
            ViewBag.ProductoID = new SelectList(db.Productos, "ProductoID", "Nombre");
            ViewBag.TipoQueRec_ID = db.TipoQuejasReclamos.Where(x => x.EsParaReclamo)
                .Select(x => new SelectListItem
                {
                    Value = x.TipoQueRec_ID.ToString(),
                    Text = x.Descripcion,
                });
            ViewBag.Estado = new SelectList(db.EstadoQuejaReclamo, "EstadoQueRec_ID", "Descripcion");
            Reclamacion rec = new Reclamacion()
            {
                Fecha = DateTime.Now,
            };
            return View(rec);
        }

        // POST: Reclamaciones/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "QuejaRecID,DepartamentoID,ClienteID,Fecha,Detalle,TipoQueRec_ID,ProductoID,EstadoQueRec_ID")] Reclamacion reclamacion)
        {
            if (ModelState.IsValid)
            {
                if (!User.IsInRole("Admin")
                    && UserManager.FindById(User.Identity.GetUserId()).ClienteAsignado?.ClienteID != null)
                {
                    reclamacion.ClienteID = UserManager.FindById(User.Identity.GetUserId()).ClienteAsignado.ClienteID;
                }
                reclamacion.EstadoQueRec_ID = db.EstadoQuejaReclamo.FirstOrDefault().EstadoQueRec_ID;
                db.QuejasReclamos.Add(reclamacion);
                db.SaveChanges();
                return RedirectToAction("Index", "QuejasReclamos");
            }

            ViewBag.ClienteID = new SelectList(db.Clientes, "ClienteID", "Nombre", reclamacion.ClienteID);
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "Nombre", reclamacion.DepartamentoID);
            ViewBag.ProductoID = new SelectList(db.Productos, "ProductoID", "Nombre", reclamacion.ProductoID);
            ViewBag.TipoQueRec_ID = new SelectList(db.TipoQuejasReclamos.Where(x => x.EsParaReclamo), "TipoQueRec_ID", "Descripcion", reclamacion.TipoQueRec_ID);
            ViewBag.EstadoQueRec_ID = new SelectList(db.EstadoQuejaReclamo, "EstadoQueRec_ID", "Descripcion", reclamacion.EstadoQueRec_ID);
            return View(reclamacion);
        }

        // GET: Reclamaciones/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reclamacion reclamacion = db.QuejasReclamos.OfType<Reclamacion>().Where(x => x.QuejaRecID == id).First();
            if (reclamacion == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClienteID = new SelectList(db.Clientes, "ClienteID", "Nombre", reclamacion.ClienteID);
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "Nombre", reclamacion.DepartamentoID);
            ViewBag.ProductoID = new SelectList(db.Productos, "ProductoID", "Nombre", reclamacion.ProductoID);
            ViewBag.TipoQueRec_ID = new SelectList(db.TipoQuejasReclamos.Where(x => x.EsParaReclamo), "TipoQueRec_ID", "Descripcion", reclamacion.TipoQueRec_ID);
            ViewBag.EstadoQueRec_ID = new SelectList(db.EstadoQuejaReclamo, "EstadoQueRec_ID", "Descripcion", reclamacion.EstadoQueRec_ID);
            return View(reclamacion);
        }

        // POST: Reclamaciones/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "QuejaRecID,DepartamentoID,ClienteID,Fecha,Detalle,TipoQueRec_ID,ProductoID,EstadoQueRec_ID")] Reclamacion reclamacion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reclamacion).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "QuejasReclamos");
            }
            ViewBag.ClienteID = new SelectList(db.Clientes, "ClienteID", "Nombre", reclamacion.ClienteID);
            ViewBag.DepartamentoID = new SelectList(db.Departamentos, "DepartamentoID", "Nombre", reclamacion.DepartamentoID);
            ViewBag.ProductoID = new SelectList(db.Productos, "ProductoID", "Nombre", reclamacion.ProductoID);
            ViewBag.TipoQueRec_ID = new SelectList(db.TipoQuejasReclamos.Where(x => x.EsParaReclamo), "TipoQueRec_ID", "Descripcion", reclamacion.TipoQueRec_ID);
            ViewBag.EstadoQueRec_ID = new SelectList(db.EstadoQuejaReclamo, "EstadoQueRec_ID", "Descripcion", reclamacion.EstadoQueRec_ID);
            return View(reclamacion);
        }

        // GET: Reclamaciones/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reclamacion reclamacion = db.QuejasReclamos.OfType<Reclamacion>().Where(x => x.QuejaRecID == id).First();
            if (reclamacion == null)
            {
                return HttpNotFound();
            }
            return View(reclamacion);
        }

        // POST: Reclamaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Reclamacion reclamacion = db.QuejasReclamos.OfType<Reclamacion>().Where(x => x.QuejaRecID == id).First();
            db.QuejasReclamos.Remove(reclamacion);
            db.SaveChanges();
            return RedirectToAction("Index", "QuejasReclamos");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
