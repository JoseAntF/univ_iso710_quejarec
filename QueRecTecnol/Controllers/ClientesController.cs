﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using QueRecTecnol.Models;

namespace QueRecTecnol.Controllers
{
    [Authorize]
    public class ClientesController : Controller
    {
        private CompDbContext db = new CompDbContext();

        // GET: Clientes

        public ActionResult Index()
        {
            var clientes = db.Clientes.Include(c => c.EstadoCliente).Include(c => c.Oficial).Include(c => c.Oficina).Include(c => c.Pais).Include(c => c.TipoIdentificacion);
            return View(clientes.ToList());
        }

        // Relacionado a Reporte //

        struct ClienteReportable
        {
            public string Nombre { get; set; }
            public string TipoIdentificacion { get; set; }
            public string Identificacion { get; set; }
            public string Telefono { get; set; }
            public string Correo { get; set; }
            public DateTime FechaCreacion { get; set; }
            public string Estado { get; set; }
            public string Oficina { get; set; }
            public string Oficial { get; set; }
            public string Provincia { get; set; }
            public string Sector { get; set; }
            public string Municipio { get; set; }
            public string Barrio { get; set; }
            public string Dir1 { get; set; }
            public string Dir2 { get; set; }
            public string Pais { get; set; }
        }

        ClienteReportable HacerClienteReportable(Cliente c)
        {
            return new ClienteReportable
            {
                Nombre = c.Nombre,
                TipoIdentificacion = c.TipoIdentificacion.Descripcion,
                Identificacion = c.Identificacion,
                Telefono = c.Telefono,
                Correo = c.Correo,
                FechaCreacion = c.FechaCreacion,
                Estado = c.EstadoCliente.Descripcion,
                Oficina = c.Oficina.Descripcion,
                Oficial = c.Oficial.Nombre,
                Provincia = c.Provincia,
                Sector = c.Sector,
                Municipio = c.Municipio,
                Barrio = c.Barrio,
                Dir1 = c.Direccion1,
                Dir2 = c.Direccion2,
                Pais = c.Pais.Nombre
            };
        }

        // GET: Clientes/Reporte
        [Authorize(Roles = "Admin")]
        public ActionResult Reporte()
        {
            StringBuilder csv = new StringBuilder();
            var clientesR = db.Clientes.Include(c => c.EstadoCliente).Include(c => c.Oficial)
                            .Include(c => c.Oficina).Include(c => c.Pais)
                            .Include(c => c.TipoIdentificacion).AsEnumerable().Select(c => HacerClienteReportable(c)).ToArray();
            csv.AppendLine("Nombre;Tipo de Ident.;Identificacion;Telefono;Correo;Creado en;Estado;Oficina;Oficial;Provincia;Sector;Municipio;Barrio;Direcc. 1;Direcc. 2;Pais");
                
            foreach(ClienteReportable cR in clientesR)
            {
                csv.AppendFormat("\"{0}\";\"{1}\";\"=\"\"{2}\"\"\";\"=\"\"{3}\"\"\";\"{4}\";\"{5}\";\"{6}\";\"{7}\";\"{8}\";\"{9}\";\"{10}\";\"{11}\";\"{12}\";\"{13}\";\"{14}\";\"{15}\"\n",
                    cR.Nombre, cR.TipoIdentificacion, cR.Identificacion,
                    cR.Telefono, cR.Correo, cR.FechaCreacion.ToString(), cR.Estado,
                    cR.Oficina, cR.Oficial, cR.Provincia, cR.Sector, cR.Municipio,
                    cR.Barrio, cR.Dir1, cR.Dir2, cR.Pais);
            }
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = "reporteCliente.csv",
                Inline = false
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            Response.Charset = "utf-8";
            var data = Encoding.UTF8.GetBytes(csv.ToString());
            var result = Encoding.UTF8.GetPreamble().Concat(data).ToArray();
            return File(result, "text/csv");
        }

        // GET: Clientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // GET: Clientes/Create
        public ActionResult Create()
        {
            ViewBag.EstadoClienteID = new SelectList(db.EstadosClientes, "EstadoClienteID", "Descripcion");
            ViewBag.EmpleadoID = new SelectList(db.Empleados, "EmpleadoID", "Nombre");
            ViewBag.OficinaID = new SelectList(db.Oficinas, "OficinaID", "Descripcion");
            ViewBag.PaisID = new SelectList(db.Paises, "PaisID", "Nombre");
            ViewBag.TipoIdentificacionID = new SelectList(db.TipoIdentificacion, "TipoIdentificacionID", "Descripcion");
            return View(new Cliente());
        }

        // POST: Clientes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ClienteID,Nombre,TipoIdentificacionID,Identificacion,Telefono,Correo,EstadoClienteID,OficinaID,EmpleadoID,Provincia,Sector,Municipio,Barrio,Direccion1,Direccion2,PaisID")] Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                cliente.FechaCreacion = DateTime.Now;
                db.Clientes.Add(cliente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.EstadoClienteID = new SelectList(db.EstadosClientes, "EstadoClienteID", "Descripcion", cliente.EstadoClienteID);
            ViewBag.EmpleadoID = new SelectList(db.Empleados, "EmpleadoID", "Nombre", cliente.EmpleadoID);
            ViewBag.OficinaID = new SelectList(db.Oficinas, "OficinaID", "Descripcion", cliente.OficinaID);
            ViewBag.PaisID = new SelectList(db.Paises, "PaisID", "Nombre", cliente.PaisID);
            ViewBag.TipoIdentificacionID = new SelectList(db.TipoIdentificacion, "TipoIdentificacionID", "Descripcion", cliente.TipoIdentificacionID);
            return View(cliente);
        }

        // GET: Clientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            ViewBag.EstadoClienteID = new SelectList(db.EstadosClientes, "EstadoClienteID", "Descripcion", cliente.EstadoClienteID);
            ViewBag.EmpleadoID = new SelectList(db.Empleados, "EmpleadoID", "Nombre", cliente.EmpleadoID);
            ViewBag.OficinaID = new SelectList(db.Oficinas, "OficinaID", "Descripcion", cliente.OficinaID);
            ViewBag.PaisID = new SelectList(db.Paises, "PaisID", "Nombre", cliente.PaisID);
            ViewBag.TipoIdentificacionID = new SelectList(db.TipoIdentificacion, "TipoIdentificacionID", "Descripcion", cliente.TipoIdentificacionID);
            return View(cliente);
        }

        // POST: Clientes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ClienteID,Nombre,TipoIdentificacionID,Identificacion,Telefono,Correo,FechaCreacion,EstadoClienteID,OficinaID,EmpleadoID,Provincia,Sector,Municipio,Barrio,Direccion1,Direccion2,PaisID")] Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cliente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.EstadoClienteID = new SelectList(db.EstadosClientes, "EstadoClienteID", "Descripcion", cliente.EstadoClienteID);
            ViewBag.EmpleadoID = new SelectList(db.Empleados, "EmpleadoID", "Nombre", cliente.EmpleadoID);
            ViewBag.OficinaID = new SelectList(db.Oficinas, "OficinaID", "Descripcion", cliente.OficinaID);
            ViewBag.PaisID = new SelectList(db.Paises, "PaisID", "Nombre", cliente.PaisID);
            ViewBag.TipoIdentificacionID = new SelectList(db.TipoIdentificacion, "TipoIdentificacionID", "Descripcion", cliente.TipoIdentificacionID);
            return View(cliente);
        }

        // GET: Clientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // POST: Clientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cliente cliente = db.Clientes.Find(id);
            db.Clientes.Remove(cliente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
