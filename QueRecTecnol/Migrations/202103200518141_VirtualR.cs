namespace QueRecTecnol.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VirtualR : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Productoes", "Cliente_ClienteID", c => c.Int());
            CreateIndex("dbo.Productoes", "Cliente_ClienteID");
            AddForeignKey("dbo.Productoes", "Cliente_ClienteID", "dbo.Clientes", "ClienteID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Productoes", "Cliente_ClienteID", "dbo.Clientes");
            DropIndex("dbo.Productoes", new[] { "Cliente_ClienteID" });
            DropColumn("dbo.Productoes", "Cliente_ClienteID");
        }
    }
}
