namespace QueRecTecnol.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VirtualR2_fix : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.EstadoQuejaReclamoes", "Descripcion", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.EstadoQuejaReclamoes", "Descripcion", c => c.Int(nullable: false));
        }
    }
}
