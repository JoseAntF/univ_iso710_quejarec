namespace QueRecTecnol.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NombresIdentity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Name", c => c.String(nullable: false, defaultValue: "Usuario"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Name");
        }
    }
}
