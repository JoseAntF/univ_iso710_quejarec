namespace QueRecTecnol.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ArregDepart : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Departamentoes", "Nombre", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Departamentoes", "Nombre", c => c.Int(nullable: false));
        }
    }
}
