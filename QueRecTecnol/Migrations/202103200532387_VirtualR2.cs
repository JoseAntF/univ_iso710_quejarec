namespace QueRecTecnol.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VirtualR2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Clientes", "Oficina_OficinaID", "dbo.Oficinas");
            DropForeignKey("dbo.Clientes", "Pais_PaisID", "dbo.Pais");
            DropIndex("dbo.Clientes", new[] { "Oficial_EmpleadoID" });
            DropIndex("dbo.Clientes", new[] { "Oficina_OficinaID" });
            DropIndex("dbo.Clientes", new[] { "Pais_PaisID" });
            RenameColumn(table: "dbo.Clientes", name: "EstadoCliente_EstadoClienteID", newName: "EstadoClienteID");
            RenameColumn(table: "dbo.Clientes", name: "Oficial_EmpleadoID", newName: "EmpleadoID");
            RenameColumn(table: "dbo.Clientes", name: "Oficina_OficinaID", newName: "OficinaID");
            RenameColumn(table: "dbo.Clientes", name: "Pais_PaisID", newName: "PaisID");
            RenameColumn(table: "dbo.Clientes", name: "TipoIdentificacion_TipoIdentificacionID", newName: "TipoIdentificacionID");
            RenameColumn(table: "dbo.QuejaReclamoBases", name: "Estado_EstadoQueRec_ID", newName: "EstadoQueRec_ID");
            RenameColumn(table: "dbo.QuejaReclamoBases", name: "Tipo_TipoQueRec_ID", newName: "TipoQueRec_ID");
            RenameIndex(table: "dbo.Clientes", name: "IX_TipoIdentificacion_TipoIdentificacionID", newName: "IX_TipoIdentificacionID");
            RenameIndex(table: "dbo.Clientes", name: "IX_EstadoCliente_EstadoClienteID", newName: "IX_EstadoClienteID");
            RenameIndex(table: "dbo.QuejaReclamoBases", name: "IX_Tipo_TipoQueRec_ID", newName: "IX_TipoQueRec_ID");
            RenameIndex(table: "dbo.QuejaReclamoBases", name: "IX_Estado_EstadoQueRec_ID", newName: "IX_EstadoQueRec_ID");
            AlterColumn("dbo.Clientes", "EmpleadoID", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "OficinaID", c => c.Int(nullable: false));
            AlterColumn("dbo.Clientes", "PaisID", c => c.Int(nullable: false));
            CreateIndex("dbo.Clientes", "OficinaID");
            CreateIndex("dbo.Clientes", "EmpleadoID");
            CreateIndex("dbo.Clientes", "PaisID");
            AddForeignKey("dbo.Clientes", "OficinaID", "dbo.Oficinas", "OficinaID", cascadeDelete: true);
            AddForeignKey("dbo.Clientes", "PaisID", "dbo.Pais", "PaisID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Clientes", "PaisID", "dbo.Pais");
            DropForeignKey("dbo.Clientes", "OficinaID", "dbo.Oficinas");
            DropIndex("dbo.Clientes", new[] { "PaisID" });
            DropIndex("dbo.Clientes", new[] { "EmpleadoID" });
            DropIndex("dbo.Clientes", new[] { "OficinaID" });
            AlterColumn("dbo.Clientes", "PaisID", c => c.Int());
            AlterColumn("dbo.Clientes", "OficinaID", c => c.Int());
            AlterColumn("dbo.Clientes", "EmpleadoID", c => c.Int());
            RenameIndex(table: "dbo.QuejaReclamoBases", name: "IX_EstadoQueRec_ID", newName: "IX_Estado_EstadoQueRec_ID");
            RenameIndex(table: "dbo.QuejaReclamoBases", name: "IX_TipoQueRec_ID", newName: "IX_Tipo_TipoQueRec_ID");
            RenameIndex(table: "dbo.Clientes", name: "IX_EstadoClienteID", newName: "IX_EstadoCliente_EstadoClienteID");
            RenameIndex(table: "dbo.Clientes", name: "IX_TipoIdentificacionID", newName: "IX_TipoIdentificacion_TipoIdentificacionID");
            RenameColumn(table: "dbo.QuejaReclamoBases", name: "TipoQueRec_ID", newName: "Tipo_TipoQueRec_ID");
            RenameColumn(table: "dbo.QuejaReclamoBases", name: "EstadoQueRec_ID", newName: "Estado_EstadoQueRec_ID");
            RenameColumn(table: "dbo.Clientes", name: "TipoIdentificacionID", newName: "TipoIdentificacion_TipoIdentificacionID");
            RenameColumn(table: "dbo.Clientes", name: "PaisID", newName: "Pais_PaisID");
            RenameColumn(table: "dbo.Clientes", name: "OficinaID", newName: "Oficina_OficinaID");
            RenameColumn(table: "dbo.Clientes", name: "EmpleadoID", newName: "Oficial_EmpleadoID");
            RenameColumn(table: "dbo.Clientes", name: "EstadoClienteID", newName: "EstadoCliente_EstadoClienteID");
            CreateIndex("dbo.Clientes", "Pais_PaisID");
            CreateIndex("dbo.Clientes", "Oficina_OficinaID");
            CreateIndex("dbo.Clientes", "Oficial_EmpleadoID");
            AddForeignKey("dbo.Clientes", "Pais_PaisID", "dbo.Pais", "PaisID");
            AddForeignKey("dbo.Clientes", "Oficina_OficinaID", "dbo.Oficinas", "OficinaID");
        }
    }
}
