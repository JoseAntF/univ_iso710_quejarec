namespace QueRecTecnol.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clientes",
                c => new
                    {
                        ClienteID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        Cedula = c.String(nullable: false, maxLength: 11),
                        Telefono = c.String(nullable: false),
                        Correo = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ClienteID);
            
            CreateTable(
                "dbo.Departamentoes",
                c => new
                    {
                        DepartamentoID = c.Int(nullable: false, identity: true),
                        Nombre = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DepartamentoID);
            
            CreateTable(
                "dbo.Empleadoes",
                c => new
                    {
                        EmpleadoID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        DepartamentoID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EmpleadoID)
                .ForeignKey("dbo.Departamentoes", t => t.DepartamentoID, cascadeDelete: true)
                .Index(t => t.DepartamentoID);
            
            CreateTable(
                "dbo.Productoes",
                c => new
                    {
                        ProductoID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                        Precio = c.Double(nullable: false),
                        Estado = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProductoID);
            
            CreateTable(
                "dbo.QuejaReclamoBases",
                c => new
                    {
                        QuejaRecID = c.Int(nullable: false, identity: true),
                        DepartamentoID = c.Int(nullable: false),
                        ClienteID = c.Int(nullable: false),
                        Fecha = c.DateTime(nullable: false),
                        Detalle = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.QuejaRecID)
                .ForeignKey("dbo.Clientes", t => t.ClienteID, cascadeDelete: true)
                .ForeignKey("dbo.Departamentoes", t => t.DepartamentoID, cascadeDelete: true)
                .Index(t => t.DepartamentoID)
                .Index(t => t.ClienteID);
            
            CreateTable(
                "dbo.Respuestas",
                c => new
                    {
                        RespuestaID = c.Int(nullable: false, identity: true),
                        EmpleadoID = c.Int(nullable: false),
                        DepartamentoID = c.Int(nullable: false),
                        QuejaRecID = c.Int(nullable: false),
                        FechaRespuesta = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.RespuestaID)
                .ForeignKey("dbo.Departamentoes", t => t.DepartamentoID, cascadeDelete: true)
                .ForeignKey("dbo.Empleadoes", t => t.EmpleadoID)
                .ForeignKey("dbo.QuejaReclamoBases", t => t.QuejaRecID)
                .Index(t => t.EmpleadoID)
                .Index(t => t.DepartamentoID)
                .Index(t => t.QuejaRecID);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Quejas",
                c => new
                    {
                        QuejaRecID = c.Int(nullable: false),
                        Tipo = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuejaRecID)
                .ForeignKey("dbo.QuejaReclamoBases", t => t.QuejaRecID)
                .Index(t => t.QuejaRecID);
            
            CreateTable(
                "dbo.Reclamos",
                c => new
                    {
                        QuejaRecID = c.Int(nullable: false),
                        Tipo = c.Int(nullable: false),
                        ProductoID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuejaRecID)
                .ForeignKey("dbo.QuejaReclamoBases", t => t.QuejaRecID)
                .ForeignKey("dbo.Productoes", t => t.ProductoID, cascadeDelete: true)
                .Index(t => t.QuejaRecID)
                .Index(t => t.ProductoID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reclamos", "ProductoID", "dbo.Productoes");
            DropForeignKey("dbo.Reclamos", "QuejaRecID", "dbo.QuejaReclamoBases");
            DropForeignKey("dbo.Quejas", "QuejaRecID", "dbo.QuejaReclamoBases");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Respuestas", "QuejaRecID", "dbo.QuejaReclamoBases");
            DropForeignKey("dbo.Respuestas", "EmpleadoID", "dbo.Empleadoes");
            DropForeignKey("dbo.Respuestas", "DepartamentoID", "dbo.Departamentoes");
            DropForeignKey("dbo.QuejaReclamoBases", "DepartamentoID", "dbo.Departamentoes");
            DropForeignKey("dbo.QuejaReclamoBases", "ClienteID", "dbo.Clientes");
            DropForeignKey("dbo.Empleadoes", "DepartamentoID", "dbo.Departamentoes");
            DropIndex("dbo.Reclamos", new[] { "ProductoID" });
            DropIndex("dbo.Reclamos", new[] { "QuejaRecID" });
            DropIndex("dbo.Quejas", new[] { "QuejaRecID" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Respuestas", new[] { "QuejaRecID" });
            DropIndex("dbo.Respuestas", new[] { "DepartamentoID" });
            DropIndex("dbo.Respuestas", new[] { "EmpleadoID" });
            DropIndex("dbo.QuejaReclamoBases", new[] { "ClienteID" });
            DropIndex("dbo.QuejaReclamoBases", new[] { "DepartamentoID" });
            DropIndex("dbo.Empleadoes", new[] { "DepartamentoID" });
            DropTable("dbo.Reclamos");
            DropTable("dbo.Quejas");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Respuestas");
            DropTable("dbo.QuejaReclamoBases");
            DropTable("dbo.Productoes");
            DropTable("dbo.Empleadoes");
            DropTable("dbo.Departamentoes");
            DropTable("dbo.Clientes");
        }
    }
}
