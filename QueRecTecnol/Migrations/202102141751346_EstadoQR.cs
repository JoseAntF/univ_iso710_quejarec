namespace QueRecTecnol.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EstadoQR : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuejaReclamoBases", "Estado", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.QuejaReclamoBases", "Estado");
        }
    }
}
