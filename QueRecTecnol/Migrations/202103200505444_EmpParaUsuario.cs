namespace QueRecTecnol.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EmpParaUsuario : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "EmpleadoAsignado_EmpleadoID", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "EmpleadoAsignado_EmpleadoID");
            AddForeignKey("dbo.AspNetUsers", "EmpleadoAsignado_EmpleadoID", "dbo.Empleadoes", "EmpleadoID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "EmpleadoAsignado_EmpleadoID", "dbo.Empleadoes");
            DropIndex("dbo.AspNetUsers", new[] { "EmpleadoAsignado_EmpleadoID" });
            DropColumn("dbo.AspNetUsers", "EmpleadoAsignado_EmpleadoID");
        }
    }
}
