namespace QueRecTecnol.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Parc2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EstadoClientes",
                c => new
                    {
                        EstadoClienteID = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.EstadoClienteID);
            
            CreateTable(
                "dbo.Oficinas",
                c => new
                    {
                        OficinaID = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false),
                        Direccion = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.OficinaID);
            
            CreateTable(
                "dbo.Pais",
                c => new
                    {
                        PaisID = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.PaisID);
            
            CreateTable(
                "dbo.TipoIdentificacions",
                c => new
                    {
                        TipoIdentificacionID = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.TipoIdentificacionID);
            
            CreateTable(
                "dbo.EstadoQuejaReclamoes",
                c => new
                    {
                        EstadoQueRec_ID = c.Int(nullable: false, identity: true),
                        Descripcion = c.Int(nullable: false),
                        CierraTiquet = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.EstadoQueRec_ID);
            
            CreateTable(
                "dbo.TipoQuejaReclamoes",
                c => new
                    {
                        TipoQueRec_ID = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(nullable: false),
                        EsParaReclamo = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.TipoQueRec_ID);
            
            AddColumn("dbo.Clientes", "Identificacion", c => c.String(nullable: false));
            AddColumn("dbo.Clientes", "FechaCreacion", c => c.DateTime(nullable: false));
            AddColumn("dbo.Clientes", "Provincia", c => c.String(nullable: false));
            AddColumn("dbo.Clientes", "Sector", c => c.String(nullable: false));
            AddColumn("dbo.Clientes", "Municipio", c => c.String(nullable: false));
            AddColumn("dbo.Clientes", "Barrio", c => c.String(nullable: false));
            AddColumn("dbo.Clientes", "Direccion1", c => c.String(nullable: false));
            AddColumn("dbo.Clientes", "Direccion2", c => c.String());
            AddColumn("dbo.Clientes", "EstadoCliente_EstadoClienteID", c => c.Int(nullable: false));
            AddColumn("dbo.Clientes", "Oficial_EmpleadoID", c => c.Int());
            AddColumn("dbo.Clientes", "Oficina_OficinaID", c => c.Int());
            AddColumn("dbo.Clientes", "Pais_PaisID", c => c.Int());
            AddColumn("dbo.Clientes", "TipoIdentificacion_TipoIdentificacionID", c => c.Int(nullable: false));
            AddColumn("dbo.QuejaReclamoBases", "Estado_EstadoQueRec_ID", c => c.Int(nullable: false));
            AddColumn("dbo.QuejaReclamoBases", "Tipo_TipoQueRec_ID", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "ClienteAsignado_ClienteID", c => c.Int());
            CreateIndex("dbo.Clientes", "EstadoCliente_EstadoClienteID");
            CreateIndex("dbo.Clientes", "Oficial_EmpleadoID");
            CreateIndex("dbo.Clientes", "Oficina_OficinaID");
            CreateIndex("dbo.Clientes", "Pais_PaisID");
            CreateIndex("dbo.Clientes", "TipoIdentificacion_TipoIdentificacionID");
            CreateIndex("dbo.QuejaReclamoBases", "Estado_EstadoQueRec_ID");
            CreateIndex("dbo.QuejaReclamoBases", "Tipo_TipoQueRec_ID");
            CreateIndex("dbo.AspNetUsers", "ClienteAsignado_ClienteID");
            AddForeignKey("dbo.Clientes", "EstadoCliente_EstadoClienteID", "dbo.EstadoClientes", "EstadoClienteID", cascadeDelete: true);
            AddForeignKey("dbo.Clientes", "Oficial_EmpleadoID", "dbo.Empleadoes", "EmpleadoID");
            AddForeignKey("dbo.Clientes", "Oficina_OficinaID", "dbo.Oficinas", "OficinaID");
            AddForeignKey("dbo.Clientes", "Pais_PaisID", "dbo.Pais", "PaisID");
            AddForeignKey("dbo.Clientes", "TipoIdentificacion_TipoIdentificacionID", "dbo.TipoIdentificacions", "TipoIdentificacionID", cascadeDelete: true);
            AddForeignKey("dbo.QuejaReclamoBases", "Estado_EstadoQueRec_ID", "dbo.EstadoQuejaReclamoes", "EstadoQueRec_ID", cascadeDelete: true);
            AddForeignKey("dbo.QuejaReclamoBases", "Tipo_TipoQueRec_ID", "dbo.TipoQuejaReclamoes", "TipoQueRec_ID", cascadeDelete: true);
            AddForeignKey("dbo.AspNetUsers", "ClienteAsignado_ClienteID", "dbo.Clientes", "ClienteID");
            DropColumn("dbo.Clientes", "Cedula");
            DropColumn("dbo.QuejaReclamoBases", "Estado");
            DropColumn("dbo.Quejas", "Tipo");
            DropColumn("dbo.Reclamos", "Tipo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reclamos", "Tipo", c => c.Int(nullable: false));
            AddColumn("dbo.Quejas", "Tipo", c => c.Int(nullable: false));
            AddColumn("dbo.QuejaReclamoBases", "Estado", c => c.Int(nullable: false));
            AddColumn("dbo.Clientes", "Cedula", c => c.String(nullable: false, maxLength: 11));
            DropForeignKey("dbo.AspNetUsers", "ClienteAsignado_ClienteID", "dbo.Clientes");
            DropForeignKey("dbo.QuejaReclamoBases", "Tipo_TipoQueRec_ID", "dbo.TipoQuejaReclamoes");
            DropForeignKey("dbo.QuejaReclamoBases", "Estado_EstadoQueRec_ID", "dbo.EstadoQuejaReclamoes");
            DropForeignKey("dbo.Clientes", "TipoIdentificacion_TipoIdentificacionID", "dbo.TipoIdentificacions");
            DropForeignKey("dbo.Clientes", "Pais_PaisID", "dbo.Pais");
            DropForeignKey("dbo.Clientes", "Oficina_OficinaID", "dbo.Oficinas");
            DropForeignKey("dbo.Clientes", "Oficial_EmpleadoID", "dbo.Empleadoes");
            DropForeignKey("dbo.Clientes", "EstadoCliente_EstadoClienteID", "dbo.EstadoClientes");
            DropIndex("dbo.AspNetUsers", new[] { "ClienteAsignado_ClienteID" });
            DropIndex("dbo.QuejaReclamoBases", new[] { "Tipo_TipoQueRec_ID" });
            DropIndex("dbo.QuejaReclamoBases", new[] { "Estado_EstadoQueRec_ID" });
            DropIndex("dbo.Clientes", new[] { "TipoIdentificacion_TipoIdentificacionID" });
            DropIndex("dbo.Clientes", new[] { "Pais_PaisID" });
            DropIndex("dbo.Clientes", new[] { "Oficina_OficinaID" });
            DropIndex("dbo.Clientes", new[] { "Oficial_EmpleadoID" });
            DropIndex("dbo.Clientes", new[] { "EstadoCliente_EstadoClienteID" });
            DropColumn("dbo.AspNetUsers", "ClienteAsignado_ClienteID");
            DropColumn("dbo.QuejaReclamoBases", "Tipo_TipoQueRec_ID");
            DropColumn("dbo.QuejaReclamoBases", "Estado_EstadoQueRec_ID");
            DropColumn("dbo.Clientes", "TipoIdentificacion_TipoIdentificacionID");
            DropColumn("dbo.Clientes", "Pais_PaisID");
            DropColumn("dbo.Clientes", "Oficina_OficinaID");
            DropColumn("dbo.Clientes", "Oficial_EmpleadoID");
            DropColumn("dbo.Clientes", "EstadoCliente_EstadoClienteID");
            DropColumn("dbo.Clientes", "Direccion2");
            DropColumn("dbo.Clientes", "Direccion1");
            DropColumn("dbo.Clientes", "Barrio");
            DropColumn("dbo.Clientes", "Municipio");
            DropColumn("dbo.Clientes", "Sector");
            DropColumn("dbo.Clientes", "Provincia");
            DropColumn("dbo.Clientes", "FechaCreacion");
            DropColumn("dbo.Clientes", "Identificacion");
            DropTable("dbo.TipoQuejaReclamoes");
            DropTable("dbo.EstadoQuejaReclamoes");
            DropTable("dbo.TipoIdentificacions");
            DropTable("dbo.Pais");
            DropTable("dbo.Oficinas");
            DropTable("dbo.EstadoClientes");
        }
    }
}
