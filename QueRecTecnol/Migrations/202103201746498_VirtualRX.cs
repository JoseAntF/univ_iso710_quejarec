namespace QueRecTecnol.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VirtualRX : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Respuestas", "Valoracion", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Respuestas", "Valoracion");
        }
    }
}
