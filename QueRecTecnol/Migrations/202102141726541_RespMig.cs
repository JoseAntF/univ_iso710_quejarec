namespace QueRecTecnol.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RespMig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Respuestas", "Detalle", c => c.String(nullable: false));
            AddColumn("dbo.Respuestas", "Inconformidad", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Respuestas", "Inconformidad");
            DropColumn("dbo.Respuestas", "Detalle");
        }
    }
}
