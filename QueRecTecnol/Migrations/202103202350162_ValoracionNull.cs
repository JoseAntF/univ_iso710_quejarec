namespace QueRecTecnol.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ValoracionNull : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Respuestas", "Valoracion", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Respuestas", "Valoracion", c => c.Int(nullable: false));
        }
    }
}
